import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
from scipy.stats import gamma
from scipy.stats import geom
from scipy.optimize import fsolve

################################
################################ path
################################

os.chdir(os.path.realpath(''))

################################
################################ Import Data 
################################
time_poi = []
time_poi_5 = []
time_poi_95 = []
time_poi_25 = []
time_poi_75 = []
time_nb = []
time_nb_5 = []
time_nb_25 = []
time_nb_75 = []
time_nb_95 = []
samp_prob_plot = [0.0025,0.005,0.0075,0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1]
ind_call = ["0025","005","0075","01","02","03","04","05","06","07","08","09","1"]


# Poisson data sets
for i in range(len(ind_call)):
    my_data = genfromtxt('poi_size_time_%s.txt' % (ind_call[i]), delimiter=',')
    time = []
    for j in range(len(my_data)):
        time.append(float(my_data[j][0]))
    
    time_poi.append(np.mean(np.asarray(time)))
    time_poi_5.append(np.percentile(np.asarray(time),5))
    time_poi_25.append(np.percentile(np.asarray(time),25))
    time_poi_75.append(np.percentile(np.asarray(time),75))
    time_poi_95.append(np.percentile(np.asarray(time),95))

# neg bin data sets
for i in range(len(ind_call)):
    my_data = genfromtxt('nb_size_time_%s.txt' % (ind_call[i]), delimiter=',')
    time = []
    for j in range(len(my_data)):
        time.append(float(my_data[j][0]))
    
    time_nb.append(np.mean(np.asarray(time)))
    time_nb_5.append(np.percentile(np.asarray(time),5))
    time_nb_25.append(np.percentile(np.asarray(time),25))
    time_nb_75.append(np.percentile(np.asarray(time),75))
    time_nb_95.append(np.percentile(np.asarray(time),95))


################################
################################ theory
################################
########## parameters
p_samp = np.arange(0.001,0.11,0.0001)

####### Infection distribution
shape_inf = 6.6
scale_inf = 0.833

########## Sampling time distribution - Gamma
shape_sampling = 12.
scale_sampling = 7./12.

########## secondary infections
R0 = 1.5

########## auxiliary
dt = 0.01
da = dt
tfin = 200.
t = np.arange(0,tfin+dt,dt)
tfin_det = 200

################################ prediction
###### extinction probability - Poisson
def ext(x):
    return(x-np.exp(R0*(x-1)))

p_ext = fsolve(ext,0.5)[0]
p_surv = 1 - p_ext

###### age dependent survival probability
pext = [p_ext]
x = dt
for i in range(int(tfin/dt)):
    pext.append(p_ext*np.exp((1-p_ext)*R0*gamma.cdf(x,shape_inf,scale=scale_inf)))
    x += dt

pext = np.asarray(pext)

# infectiousness vector
tau = []
x = 0
for i in range(int(tfin/dt)+1):
    tau.append(R0*(gamma.pdf(x+dt, shape_inf, scale=scale_inf)))
    x = x + dt

tau = np.asarray(tau)

#### Incidence delayed equation - adjusted
inc = np.zeros((int(tfin/dt)+1))
inc[0] = tau[0]

for i in range(len(inc)):
    # adjustment of transmission rate
    print(i)
    integral = 0
    if (i>=1):
        for k in range(i):
            integral += np.log(pext[i-k-1])*inc[k+1]*dt
        
        tau_adj = (1-pext[0]*pext[i]*np.exp(integral))/(1-pext[i]*np.exp(integral))
        
        inc[i+1] = tau_adj * (tau[i] + np.sum( tau[i-1::-1] * inc[1:i+1] * dt ) )
    
    else:
        inc[i+1] = (1-pext[0]*pext[i])/(1-pext[i]) * tau[i] 


I_tot = []
for i in range(len(inc)):
    I_tot.append(1+np.sum(inc[1:i+1])*dt)

tot = np.asarray(I_tot)

######## sampling times
prediction = []

for j in range(len(p_samp)):
    samp_time_dist = np.zeros(len(t))
    print(j)
    #################### computation
    for k in range(1500):            ### sum over geometric distribution
        
        index = np.min(np.where(tot >=k+1))
        
        # add sampling time distribution to tdet
        samp_time_dist[index:] += geom.pmf(k+1,p_samp[j]) * gamma.pdf(t[0:(len(t)-index)],shape_sampling,scale = scale_sampling)
    
    prediction.append(np.sum(dt*t*samp_time_dist))

################################
################################ Plot
################################
plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})


plt.plot(p_samp[15:-50],prediction[15:-50],linewidth=3,color='black')
plt.plot(samp_prob_plot,time_poi,'o',color='C0',markersize=10)
plt.plot(samp_prob_plot,time_nb,'o',color='C1',markersize=10)
#plt.fill_between(det_prob_plot,time_poi_5,time_poi_95,alpha=0.25,color='C0',linewidth=0)
#plt.fill_between(det_prob_plot,time_nb_5,time_nb_95,alpha=0.25,color='C1',linewidth=0)
plt.plot(samp_prob_plot,time_poi_5,color='C0',linewidth=2,linestyle='dashed')
plt.plot(samp_prob_plot,time_poi_95,color='C0',linewidth=2,linestyle='dashed')
plt.plot(samp_prob_plot,time_nb_5,color='C1',linewidth=2,linestyle='dashed')
plt.plot(samp_prob_plot,time_nb_95,color='C1',linewidth=2,linestyle='dashed')
plt.fill_between(samp_prob_plot,time_poi_25,time_poi_75,alpha=0.25,color='C0',linewidth=0)
plt.fill_between(samp_prob_plot,time_nb_25,time_nb_75,alpha=0.25,color='C1',linewidth=0)
plt.plot(p_samp,[20]*len(p_samp),color='black',linestyle='dotted',linewidth=2)
plt.plot(p_samp,[51]*len(p_samp),color='black',linestyle='dotted',linewidth=2)
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.xlim((0,0.1025))
plt.savefig("fig2_blank.png")
plt.show()



