import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
from scipy.stats import gamma
from scipy.stats import geom
from scipy.optimize import fsolve

################################
################################ path
################################

os.chdir(os.path.realpath(''))

################################
################################ Import Data 
################################
time = []
size = []

my_data = genfromtxt('size_time.txt', delimiter=',')
#my_data = genfromtxt('size_time_poisson.txt', delimiter=',')
for i in range(len(my_data)):
    size.append(float(my_data[i][1]))
    time.append(float(my_data[i][0]))

time = np.array(time)
size = np.array(size)

################################
################################ theory
################################
########## parameters
#p_det = 0.042
p_samp = 0.0105

####### Infection distribution
shape_inf = 6.6
scale_inf = 0.833

########## Hospitalization time distribution - Gamma
shape_sampling = 12.
scale_sampling = 7./12.

########## secondary infections
R0 = 1.5

########## auxiliary
dt = 0.01
da = dt
tfin = 200.
t = np.arange(0,tfin+dt,dt)
tfin_det = 200

################################ prediction
###### extinction probability - Poisson
def ext(x):
    return(x-np.exp(R0*(x-1)))

p_ext = fsolve(ext,0.5)[0]
p_surv = 1 - p_ext

############### delay equation approach
###### extinction probability - Poisson
def ext(x):
    return(x-np.exp(R0*(x-1)))

p_ext = fsolve(ext,0.5)[0]
p_surv = 1 - p_ext

###### age dependent survival probability
pext = [p_ext]
x = dt
for i in range(int(tfin/dt)):
    pext.append(p_ext*np.exp((1-p_ext)*R0*gamma.cdf(x,shape_inf,scale=scale_inf)))
    x += dt

pext = np.asarray(pext)

# infectiousness vector
tau = []
x = 0
for i in range(int(tfin/dt)+1):
    tau.append(R0*(gamma.pdf(x+dt, shape_inf, scale=scale_inf)))
    x = x + dt

tau = np.asarray(tau)

#### Incidence delayed equation - adjusted
inc = np.zeros((int(tfin/dt)+1))
inc[0] = tau[0]

for i in range(len(inc)):
    # adjustment of transmission rate
    print(i)
    integral = 0
    if (i>=1):
        for k in range(i):
            integral += np.log(pext[i-k-1])*inc[k+1]*dt
        
        tau_adj = (1-pext[0]*pext[i]*np.exp(integral))/(1-pext[i]*np.exp(integral))
        
        inc[i+1] = tau_adj * (tau[i] + np.sum( tau[i-1::-1] * inc[1:i+1] * dt ) )
    
    else:
        inc[i+1] = (1-pext[0]*pext[i])/(1-pext[i]) * tau[i] 


I_tot = []
for i in range(len(inc)):
    I_tot.append(1+np.sum(inc[1:i+1])*dt)

tot = np.asarray(I_tot)

######## sampling times
samp_time_dist = np.zeros(len(t))

#################### computation
for k in range(1500):            ### sum over geometric distribution
   
    index = np.min(np.where(tot >=k+1))
    
    # add sampling time distribution to tdet
    samp_time_dist[index:] += geom.pmf(k+1,p_samp) * gamma.pdf(t[0:(len(t)-index)],shape_sampling,scale = scale_sampling)
  

################################
################################ Plot
################################
plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

plt.hist(time, bins=np.arange(0,np.max(time),1),density=True,alpha=0.5,color='C0')
plt.plot(t,samp_time_dist,color='black',linewidth=3)
plt.axvline(np.mean(time),color='C0',linewidth=3,linestyle='dotted')
plt.axvline(np.sum(dt*t*samp_time_dist),color='black',linewidth=3,linestyle='dashed')
plt.xlim((0,100))
plt.ylim((0,0.05))
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.savefig("fig3a_blank.png")
plt.show()


################################
################################ Mean and SD of data
################################
print(np.mean(time))
print(np.sqrt(np.var(time)))

