import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
from scipy.stats import gamma
from scipy.stats import geom
from scipy.optimize import fsolve

################################
################################ path
################################

os.chdir(os.path.realpath(''))

################################
################################ Import Data 
################################
time = []
size = []

my_data = genfromtxt('size_time.txt', delimiter=',')
#my_data = genfromtxt('size_time_poisson.txt', delimiter=',')
for i in range(len(my_data)):
    size.append(float(my_data[i][1]))
    time.append(float(my_data[i][0]))


time = np.array(time)
size = np.array(size)

################################
################################ theory
################################
########## parameters
#p_hosp = 0.042
p_samp = 0.0105

####### Infection distribution
shape_inf = 6.6
scale_inf = 0.833

########## Hospitalization time distribution - Gamma
shape_sampling = 12.
scale_sampling = 7./12.

########## secondary infections
R0 = 1.5

########## auxiliary
dt = 0.01
da = dt
tfin = 200.
t = np.arange(0,tfin+dt,dt)

################################ prediction
###### extinction probability - Poisson
def ext(x):
    return(x-np.exp(R0*(x-1)))

p_ext = fsolve(ext,0.5)[0]
p_surv = 1 - p_ext

############### delay equation approach
###### extinction probability - Poisson
def ext(x):
    return(x-np.exp(R0*(x-1)))

p_ext = fsolve(ext,0.5)[0]
p_surv = 1 - p_ext

###### age dependent survival probability
pext = [p_ext]
x = dt
for i in range(int(tfin/dt)):
    pext.append(p_ext*np.exp((1-p_ext)*R0*gamma.cdf(x,shape_inf,scale=scale_inf)))
    x += dt

pext = np.asarray(pext)

# infectiousness vector
tau = []
x = 0
for i in range(int(tfin/dt)+1):
    tau.append(R0*(gamma.pdf(x+dt, shape_inf, scale=scale_inf)))
    x = x + dt

tau = np.asarray(tau)

#### Incidence delayed equation - adjusted
inc = np.zeros((int(tfin/dt)+1))
inc[0] = tau[0]

for i in range(len(inc)):
    # adjustment of transmission rate
    print(i)
    integral = 0
    if (i>=1):
        for k in range(i):
            integral += np.log(pext[i-k-1])*inc[k+1]*dt
        
        tau_adj = (1-pext[0]*pext[i]*np.exp(integral))/(1-pext[i]*np.exp(integral))
        
        inc[i+1] = tau_adj * (tau[i] + np.sum( tau[i-1::-1] * inc[1:i+1] * dt ) )
    
    else:
        inc[i+1] = (1-pext[0]*pext[i])/(1-pext[i]) * tau[i] 


I_tot = []
for i in range(len(inc)):
    I_tot.append(1+np.sum(inc[1:i+1])*dt)

tot = np.asarray(I_tot)


######## sampling times
samp_time_dist = np.zeros(len(t))

#################### computation
for k in range(1000):            ### sum over geometric distribution
    
    # add sampling time distribution to tdet
    index = np.min(np.where(tot >=k+1))
    samp_time_dist[index:] += geom.pmf(k,p_samp) * gamma.pdf(t[0:(len(t)-index)],shape_sampling,scale = scale_sampling)


####### epidemic size distribution
dsize = 8
I = np.arange(dsize,15001,dsize)
dens_I = np.zeros(len(I))

ind_old = 0
for i in range(len(I)):
    tdet_index = np.min(np.where(tot >=I[i]))
        
    dens_I[i] = np.sum(samp_time_dist[ind_old:tdet_index]*dt)/dsize
    
    ind_old = tdet_index    

################################
################################ Plot
################################
plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

plt.hist(size, bins=np.arange(0,np.max(size),dsize),density=True,alpha=0.5,color='C0')
plt.plot(I-dsize/2,dens_I,linewidth=5,color='black')
plt.axvline(np.mean(size),color='C0',linewidth=3,linestyle='dotted')
plt.axvline(np.sum((I-dsize/2)*dsize*dens_I),color='black',linewidth=3,linestyle='dashed')
plt.xlim((0,500))
plt.ylim((0,0.01))
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.savefig("fig3b_blank.png")
plt.show()

    
################################
################################ Size statistics
################################
print(np.mean(size))
print(np.sqrt(np.var(size)))
print(np.percentile(size,95))


