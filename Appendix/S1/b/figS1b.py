import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.optimize import fsolve

################################
################################ theory
################################

########## secondary infections
R0 = np.arange(0.01,5,0.01)
kappa = 0.57

####### extinction probability (poisson)
p_est_poi = []

for i in range(len(R0)):
    def ext(x):
        return(x-np.exp(R0[i]*(x-1)))
    p_est_poi.append(1-fsolve(ext,0.5)[0])

####### extinction probability (neg bin)
p_est_nb = []

for i in range(len(R0)):    
    p_inf = kappa/(kappa+R0[i])
    def ext(x):
        return(x-(p_inf/(1-(1-p_inf)*x))**kappa)
    
    p_est_nb.append(1-fsolve(ext,0.5)[0])

####### extinction probability (geometric)
p_est_geo = []

for i in range(len(R0)):
    p_est_geo.append(1-min(1,1/R0[i]))


################################
################################ plot
################################
plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

plt.plot(R0,p_est_poi,linewidth=3)
plt.plot(R0,p_est_nb,linewidth=3)
plt.plot(R0,p_est_geo,linewidth=3)
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.savefig("fig1b_blank.png")
plt.show()

