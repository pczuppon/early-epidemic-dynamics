import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import poisson
from scipy.stats import nbinom

R0 = 2.7
kappa = 0.57
psuc = kappa/(kappa+R0)

n = np.arange(0,11,1)

plt.plot(n,poisson.pmf(n,R0),'o')
plt.plot(n+2/10,nbinom.pmf(n,kappa,psuc),'o')
plt.plot(n+4/10,nbinom.pmf(n,1,1/(1+R0)),'o')
plt.vlines(n,0,poisson.pmf(n,R0),colors='C0',lw=5,alpha=0.5)
plt.vlines(n+2/10,0,nbinom.pmf(n,kappa,psuc),colors='C1',lw=5,alpha=0.5)
plt.vlines(n+4/10,0,nbinom.pmf(n,1,1/(1+R0)),colors='C2',lw=5,alpha=0.5)
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.show()
