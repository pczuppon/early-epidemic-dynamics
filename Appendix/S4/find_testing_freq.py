import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
import pandas as pd 
from scipy.stats import gamma
from scipy.stats import geom
from scipy.stats import uniform
from scipy.optimize import fsolve

################################
################################ path
################################

os.chdir(os.path.realpath(''))

################################
################################ Import Data 
################################
time = []
size = []

my_data = genfromtxt('size_time.txt', delimiter=',')
#my_data = genfromtxt('size_time_poisson.txt', delimiter=',')
for i in range(len(my_data)):
    size.append(float(my_data[i][1]))
    time.append(float(my_data[i][0]))


time = np.array(time)
size = np.array(size)

################################
################################ theory
################################

####### Infection distribution
shape_inf = 6.6
scale_inf = 0.833

########## secondary infections
R0 = 1.1

########## auxiliary
dt = 0.01
da = dt
tfin = 200.
t = np.arange(0,tfin+dt,dt)

################################ Detection probabilities
data = pd.read_csv("hellewell_fig3_S1b_raw.csv",dtype={"proba_positive": np.float32},skiprows=0) 
p_pos = data.values[:,1]
normalized = p_pos/np.sum(p_pos)

###### make a continuous distribution from the data
detect_dist = []
k = 0
j = 0
int_time = 0.
for i in range(len(t)):
    if (j%10 == 0):
        k+=1
        int_time = 0.
    if (k < len(normalized)-1):
        detect_dist.append((normalized[k]-normalized[k-1])*int_time/.1 + normalized[k-1])
    elif (k==len(normalized)-1):
        detect_dist.append((0-normalized[k])*int_time/.1 + normalized[k])
    else:
        detect_dist.append(0)
    int_time += dt
    j+=1

detect_dist = np.asarray(detect_dist)
detect_dist = detect_dist/(np.sum(detect_dist)*dt)

################################ prediction
###### extinction probability - Poisson
def ext(x):
    return(x-np.exp(R0*(x-1)))

p_ext = fsolve(ext,0.5)[0]
p_surv = 1 - p_ext

############### delay equation approach
pext = [p_ext]
x = dt
for i in range(int(tfin/dt)-1):
    pext.append(p_ext*np.exp((1-p_ext)*R0*gamma.cdf(x,shape_inf,scale=scale_inf)))
    x += dt

pext = np.asarray(pext)

# infectiousness vector
tau = []
x = 0
for i in range(int(tfin/dt)):
    tau.append(R0*(gamma.pdf(x+dt/2, shape_inf, scale=scale_inf)))
    x = x + dt

tau = np.asarray(tau)

#### Incidence delayed equation - adjusted
inc = np.zeros((int(tfin/dt)+1))
inc[0] = tau[0]

for i in range(len(inc)):
    # adjustment of transmission rate
    print(i)
    integral = 0
    if (i>=1):
        for k in range(i):
            integral += np.log(pext[i-k-1])*inc[k+1]*dt
        
        tau_adj = (1-pext[0]*pext[i]*np.exp(integral))/(1-pext[i]*np.exp(integral))
        
        inc[i+1] = tau_adj * (tau[i] + np.sum( tau[i-1::-1] * inc[1:i+1] * dt ) )
    
    else:
        inc[i+1] = (1-pext[0]*pext[i])/(1-pext[i]) * tau[i] 


I_tot = []
for i in range(len(inc)):
    I_tot.append(1+np.sum(inc[1:i+1])*dt)

tot = np.asarray(I_tot)


####################### Optimization
#######################
p_detect = 0.1         # detection probability

######## detection times
det_time_dist = np.zeros(len(t))

#################### computation
for k in range(500):            ### sum over geometric distribution
    
    # add detection time distribution to tdet
    index = np.min(np.where(tot >=k+1))
    det_time_dist[index:] += geom.pmf(k,p_detect) * detect_dist[0:(len(t)-index)]


####### epidemic size distribution
dsize = 5
I = np.arange(dsize,1500,dsize)
dens_I = np.zeros(len(I))

ind_old = 0
for i in range(len(I)):
    tdet_index = np.min(np.where(tot >=I[i]))
        
    dens_I[i] = np.sum(det_time_dist[ind_old:tdet_index]*dt)/dsize
    
    ind_old = tdet_index    


################# finding optimal value iteratively
dp = 0.001
thresh = 30

while (np.sum((I-dsize/2)*dsize*dens_I) < 30):
    print(np.sum((I-dsize/2)*dsize*dens_I))
    p_detect -= dp
    
    ######## detection times
    det_time_dist = np.zeros(len(t))
    
    #################### computation
    for k in range(200):            ### sum over geometric distribution
        
        # add detection time distribution to tdet
        index = np.min(np.where(tot >=k+1))
        det_time_dist[index:] += geom.pmf(k,p_detect) * detect_dist[0:(len(t)-index)]
    
    
    ####### epidemic size distribution
    dens_I = np.zeros(len(I))
    
    ind_old = 0
    for i in range(len(I)):
        tdet_index = np.min(np.where(tot >=I[i]))
            
        dens_I[i] = np.sum(det_time_dist[ind_old:tdet_index]*dt)/dsize
        
        ind_old = tdet_index

p_detect += dp

################################ Daily detection probabilities
data2 = pd.read_csv("hellewell_fig3_S1b.csv",dtype={"proba_positive": np.float32},skiprows=0) 
p_pos2 = data2.values[:,1]
s = np.sum(p_pos2)

p_detect/s




