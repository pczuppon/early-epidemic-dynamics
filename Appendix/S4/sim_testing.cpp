#include <iostream>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <fstream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

// g++ -std=c++11 sim_testing.cpp `pkg-config --libs gsl` command for compiling

using namespace std;

////////////////////////////////////////
// parameters
////////////////////////////////////////

// secondary infections distribution - negative binomial
#define kappa 0.57                  // dispersion parameter
#define R0 1.1                       // mean number of secondary infections
#define p_neg kappa/(kappa+R0)      // success probability for the negative-binomial      

// Infectiousness distribution - gamma distribution (mean = 6 days, SD = 2.5 days)
#define shape_inf 6.6
#define scale_inf 0.833

// individual detection probability
#define sum_p_detect 3.6

// Repetitions
#define repeats 10000                             // number of repetitions

// Random number generation with Mersenne Twister
gsl_rng * r = gsl_rng_alloc (gsl_rng_mt19937);

////////////////////////////////////////
// Main code
////////////////////////////////////////

int RUN(int,double *det_dist_ptr,double p_detect);
// int argc,char *argv[]
int main()
{
    int r_ind = 0;                                    // repetition index
    int success = 0;                                  // epidemic counter
    
    // detection probability
    double p_detect = sum_p_detect * 0.0225;
    
    // Loading detection time distribution
    ifstream inFile;

    inFile.open("detect_dist.txt");
    
    double det_dist[20000] = {0};
    double x;
    int i = 0;
    while (inFile >> x) {
        det_dist[i] = x;
        i++;
    }
    double *det_dist_ptr = det_dist;
        
    while (success < repeats)
    {
        gsl_rng_set(r,r_ind);                  // setting the seed
        success += RUN(r_ind,det_dist_ptr,p_detect);                 // Run stochastic simulation
        r_ind++;
    }
        
    return(0);
}

////////////////////////////////////////
// Stochastic simulation
////////////////////////////////////////
int RUN(int r_ind,double *det_dist_ptr,double p_detect)
{
    double det_min = 1000.;               // minimal detection time -- 1000 is a dummy number!
    double t=0;                             // time
    double dt = 0.1;                        // time step !!! WARNING: Only values of format 10^(-k) allowed
    int length = (int)det_min/dt;         // array length
    int return_value = 0;
    int index = 0;
    
    int I_work[length+1] = {0};             // number of infecteds working vector
    int I[(int)det_min+1] = {0};          // number of infecteds at a certain time (days!)
    
    // Initialization = 1 infected individual at time 0
    I_work[0] = 1;
    I[0] = 1;
    
    // Random distributions necessary throughout
    //unsigned int gsl_ran_negative_binomial(const gsl_rng * rrest, double p, double n);  // p = success probability, n = sample size
    unsigned int gsl_ran_poisson(const gsl_rng * r, double mu);  // mu = mean
    double gsl_ran_gamma(const gsl_rng * rrest, double a, double b);        // a = shape, b = scale
    double gsl_ran_lognormal(const gsl_rng * r, double zeta, double sigma); // zeta = mean, sigma = standard deviation
    unsigned int gsl_ran_binomial(const gsl_rng * rbin, double p, unsigned int n); // p = prob, n = size
    double gsl_ran_flat(const gsl_rng * r, double a, double b);     // a = t_min, b = t_max
    
    int work_ind = 0;
    while (t < det_min)
    {   
        // check for detection of individuals added at time t
        int n_det = gsl_ran_binomial(r, p_detect, I_work[work_ind]);
        
        // update minimal detection time
        for (int i = 1; i <= n_det; i++)
        {
            double r_unif = gsl_ran_flat(r,0.,1.);
            int upd = 0;
            int j = 1;
            while (upd==0)
            {
                if (r_unif < accumulate(det_dist_ptr,det_dist_ptr + j,0.)/accumulate(det_dist_ptr,det_dist_ptr + 20000,0.))  upd = 1;
                else j++;   
            }
            
            double det_time = t + (double)j*0.01;
            if (det_time < det_min)
            {
                det_min = det_time;
                index = work_ind;
            }
        }
        
        // no. of offspring of individuals added at time t
        int offspring = 0;
        for (int i = 1; i <= I_work[work_ind]; i++)
        {
            offspring += gsl_ran_poisson(r,R0);
        }
        
        // loop over offsprings from individuals that were added at time t
        for (int i = 1; i <= offspring; i++)
        {   
            double inf_time = t+gsl_ran_gamma(r,shape_inf,scale_inf);
            // IMPORTANT THAT dt = 10^(-k)! otherwise adapt this rounding formula!
            int inf_time_ind = roundf(inf_time/dt);
            int I_ind = ceilf(inf_time);
            
            if (inf_time_ind < length)
            {
                I_work[inf_time_ind]++;
            }
            
            if (I_ind < (int)det_min)
            {
                I[I_ind]++;
            }            
        }
        
        t += dt;
        work_ind++;
    }
       
    // If epidemic happened increase success counter
    if (det_min < 1000)
        return_value = 1;
    
    // If epidemic happened, save final pop size and time
    if (return_value ==1)
    {        
        ofstream file ("size_time_8.txt", ios::app);
        file << det_min;    // time
        file << ",";
        file << accumulate(I_work,I_work+work_ind,0);       // size
        file << "\n";
        file.close();
    }

    return(return_value);
}





