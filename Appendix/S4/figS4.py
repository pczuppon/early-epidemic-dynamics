import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
import pandas as pd 
from scipy.stats import gamma
from scipy.stats import geom
from scipy.stats import uniform
from scipy.optimize import fsolve

####### Infection distribution
shape_inf = 6.6
scale_inf = 0.833

########## secondary infections
R0 = 1.1

########## auxiliary
dt = 0.01
da = dt
tfin = 200.
t = np.arange(0,tfin+dt,dt)

################################
################################ path
################################

os.chdir(os.path.realpath(''))

################################
################################ Import Data 
################################
ind = np.arange(0,9,1)
avg = []
low = []
up = []

#my_data = genfromtxt('size_time_4.txt', delimiter=',')
#np.mean(my_data[:,1])

for i in range(len(ind)):
    my_data = genfromtxt('size_time_%s.txt' % ind[i], delimiter=',')
    avg.append(np.mean(my_data[:,1]))
    low.append(np.percentile(my_data[:,1],5))
    up.append(np.percentile(my_data[:,1],95))


################################ Detection probabilities
data = pd.read_csv("hellewell_fig3_S1b_raw.csv",dtype={"proba_positive": np.float32},skiprows=0) 
p_pos = data.values[:,1]
normalized = p_pos/np.sum(p_pos)

###### make a continuous distribution from the data
detect_dist = []
k = 0
j = 0
int_time = 0.
for i in range(len(t)):
    if (j%10 == 0):
        k+=1
        int_time = 0.
    if (k < len(normalized)-1):
        detect_dist.append((normalized[k]-normalized[k-1])*int_time/.1 + normalized[k-1])
    elif (k==len(normalized)-1):
        detect_dist.append((0-normalized[k])*int_time/.1 + normalized[k])
    else:
        detect_dist.append(0)
    int_time += dt
    j+=1

detect_dist = np.asarray(detect_dist)
detect_dist = detect_dist/(np.sum(detect_dist)*dt)

# writing detect_dist.txt
#with open("detect_dist.txt", 'a') as out:
#    for i in range(len(detect_dist)):
#        print(str(detect_dist[i]),file=out )

################################
################################ theory
################################

################################ prediction
###### extinction probability - Poisson
def ext(x):
    return(x-np.exp(R0*(x-1)))

p_ext = fsolve(ext,0.5)[0]
p_surv = 1 - p_ext

############### delay equation approach
pext = [p_ext]
x = dt
for i in range(int(tfin/dt)-1):
    pext.append(p_ext*np.exp((1-p_ext)*R0*gamma.cdf(x,shape_inf,scale=scale_inf)))
    x += dt

pext = np.asarray(pext)

# infectiousness vector
tau = []
x = 0
for i in range(int(tfin/dt)):
    tau.append(R0*(gamma.pdf(x+dt/2, shape_inf, scale=scale_inf)))
    x = x + dt

tau = np.asarray(tau)

#### Incidence delayed equation - adjusted
inc = np.zeros((int(tfin/dt)+1))
inc[0] = tau[0]

for i in range(len(inc)):
    # adjustment of transmission rate
    print(i)
    integral = 0
    if (i>=1):
        for k in range(i):
            integral += np.log(pext[i-k-1])*inc[k+1]*dt
        
        tau_adj = (1-pext[0]*pext[i]*np.exp(integral))/(1-pext[i]*np.exp(integral))
        
        inc[i+1] = tau_adj * (tau[i] + np.sum( tau[i-1::-1] * inc[1:i+1] * dt ) )
    
    else:
        inc[i+1] = (1-pext[0]*pext[i])/(1-pext[i]) * tau[i] 


I_tot = []
for i in range(len(inc)):
    I_tot.append(1+np.sum(inc[1:i+1])*dt)

tot = np.asarray(I_tot)


####################### Optimization
#######################
p_detect = np.arange(0.005,0.1,0.001)         # detection probability

avg_size = []

dsize = 5
I = np.arange(dsize,1500,dsize)

for i in range(len(p_detect)):
    
    ######## detection times
    det_time_dist = np.zeros(len(t))
    
    #################### computation
    for k in range(500):            ### sum over geometric distribution
        
        # add detection time distribution to tdet
        index = np.min(np.where(tot >=k+1))
        det_time_dist[index:] += geom.pmf(k,p_detect[i]) * detect_dist[0:(len(t)-index)]
    
    
    ####### epidemic size distribution
    dens_I = np.zeros(len(I))
    
    ind_old = 0
    for i in range(len(I)):
        tdet_index = np.min(np.where(tot >=I[i]))
            
        dens_I[i] = np.sum(det_time_dist[ind_old:tdet_index]*dt)/dsize
        
        ind_old = tdet_index
    
    avg_size.append(np.sum((I-dsize/2)*dsize*dens_I))

############## Plot
plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  # red   with alpha = 30%
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  # green with alpha = 50%
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

p_det_plot = np.arange(0.0025,0.025,0.0025)
#p_det_plot = np.asarray([0.0025,0.005,0.0075,0.01])
plt.plot(p_detect/3.6,avg_size,color='black',linewidth=3)
plt.plot(p_det_plot,avg,'o',color='C0')
plt.fill_between(p_det_plot,low,up,alpha=0.25,color='C0')
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.xlim((0,0.025))
plt.savefig("figS4_blank.png")
plt.show()




