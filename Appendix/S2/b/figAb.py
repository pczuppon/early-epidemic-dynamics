import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
from scipy.stats import gamma
from scipy.optimize import fsolve

################################
################################ path
################################

os.chdir(os.path.realpath(''))

################################
################################ Import Data 
################################
repeats = 10000

################################ 
tfin  = 80
count = np.arange(0,repeats,1)
time = np.arange(0,tfin+1,1)
size = [ [0]*len(time)] * len(count)

for i in range(len(count)):
    my_data = genfromtxt('trajectory_tfin_%s_id_%s.txt' % (tfin,count[i]), delimiter=',')
    size[i] = my_data[0:tfin+1,1]

means = []
low = []
up = []
iqr_low = []
iqr_high = []
for i in range(len(time)):
    s = []
    for j in range(len(count)):
        s.append(size[j][i])
    
    s_work = np.array(s)
    means.append(np.mean(s_work))
    low.append(np.percentile(s_work,5))
    up.append(np.percentile(s_work,95))
    iqr_low.append(np.percentile(s_work,25))
    iqr_high.append(np.percentile(s_work,75))

time = np.arange(0,80+1,1)


plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

plt.semilogy(time,means[0:len(time)],'o',color='C1')
plt.ylim((10**0,1000))
plt.xlim((0,80))
plt.fill_between(time,low[0:len(time)],up[0:len(time)],alpha=0.25,color='C1')
plt.fill_between(time,iqr_low[0:len(time)],iqr_high[0:len(time)],alpha=0.25,color='C1')
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.savefig("figA1b_blank.png")
plt.show()






