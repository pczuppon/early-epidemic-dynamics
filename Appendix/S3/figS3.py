import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
import pandas as pd 
from scipy.stats import gamma
from scipy.optimize import fsolve

################################
################################ path
################################

os.chdir(os.path.realpath(''))


################################
################################ Import Data 
################################

################################ Detection probabilities
######### Hellewell et al. data
data = pd.read_csv("hellewell_fig3_S1b.csv",dtype={"proba_positive": np.float32},skiprows=0) 
p_detect = data.values[:,-1]     

data = pd.read_csv("hellewell_fig3_S1b_raw.csv",dtype={"proba_positive": np.float32},skiprows=0) 

time = np.arange(0,30+1,1)

######### Kucirka et al. data
data_kuc = pd.read_csv("kucirka_fig2a.csv",dtype={"proba_positive": np.float32},skiprows=0) 
p_detect_kuc = data_kuc.values[:,-1]     
p_detect_kuc = np.concatenate((p_detect_kuc,np.zeros(3)))

time_kuc = np.arange(0,23+1,1)

plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})


########### plot truncated age dist
# R = 2.9
r = 0.210157

# R = 1.3
r2 = 0.0486829

plt.plot(time,p_detect,'o',color='black',markersize=7)
plt.plot(time_kuc,p_detect_kuc,'x',color='black',markersize=7)
plt.plot(np.arange(0,30.1,0.1),data.values[:,1],linewidth=3,color='black')
plt.plot(time[0:15],r*np.exp(-r*time[0:15])/(1-np.exp(-r*14)),color='black',linewidth=2,linestyle='dashed')
plt.plot(time[0:15],r2*np.exp(-r2*time[0:15])/(1-np.exp(-r2*14)),color='black',linewidth=2,linestyle='dotted')
plt.ylim((0,1))
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.savefig("figS3_blank.png")
plt.show()






