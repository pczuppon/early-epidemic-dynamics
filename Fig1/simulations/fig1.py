import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
from scipy.stats import gamma
from scipy.optimize import fsolve

################################
################################ path
################################

os.chdir(os.path.realpath(''))

################################
################################ theory
################################
####### Infection distribution
shape_inf = 6.6
scale_inf = 0.833

########## secondary infections
R0 = 1.3

####### extinction probability (poisson)
def ext(x):
    return(x-np.exp(R0*(x-1)))

p_ext = fsolve(ext,0.5)[0]
p_surv = 1 - p_ext

####### asymptotic prediction
alpha = ((1/R0)**(-1/shape_inf)-1)/(scale_inf)       
beta = shape_inf * scale_inf * R0* (1 + scale_inf * alpha)**(-1-shape_inf)
I_plot =  []
dt = 0.01
da = 0.01
tfin = 80
t = np.arange(0,tfin+dt,dt)

for i in range(len(t)):
    I_plot.append( np.exp(alpha*t[i]) / (alpha*beta*p_surv))


############### McKendrick-von Foerster approach
###### age dependent survival probability
pext = [p_ext]
x = dt
for i in range(int(tfin/dt)):
    pext.append(p_ext*np.exp((1-p_ext)*R0*gamma.cdf(x,shape_inf,scale=scale_inf)))
    x += dt

pext = np.asarray(pext)

# infectiousness vector
mu = []
x = 0
for i in range(int(tfin/dt)+1):
    mu.append(R0*(gamma.pdf(x+dt, shape_inf, scale=scale_inf)))
    x = x + dt

tau = np.asarray(mu)


#### Incidence delayed equation - adjusted
inc = np.zeros((int(tfin/dt)+1))
inc[0] = tau[0]

for i in range(len(inc)):
    # adjustment of transmission rate
    print(i)
    integral = 0
    if (i>=1):
        for k in range(i):
            integral += np.log(pext[i-k-1])*inc[k+1]*dt
        
        tau_adj = (1-pext[0]*pext[i]*np.exp(integral))/(1-pext[i]*np.exp(integral))
        
        inc[i+1] = tau_adj * (tau[i] + np.sum( tau[i-1::-1] * inc[1:i+1] * dt ) )
    
    else:
        inc[i+1] = (1-pext[0]*pext[i])/(1-pext[i]) * tau[i] 
    

I_tot = []
for i in range(len(inc)):
    I_tot.append(1+np.sum(inc[1:i+1])*dt)


##### deterministic solution - renewal equation
I_det = np.zeros(int(tfin/dt)+1)

# initialize with 1 individual at time 0 with age 0
I_det[0] = 1


for i in range(int(tfin/dt)):
    # add infections
    I_det[i+1] = I_det[0] + np.sum(I_det[0:i+1]*mu[i::-1])*dt



################################
################################ Import Data 
################################
repeats = 10000

################################ 
count = np.arange(0,repeats,1)
time = np.arange(0,tfin+1,1)
size = [ [0]*len(time)] * len(count)

for i in range(len(count)):
    my_data = genfromtxt('trajectory_tfin_%s_id_%s.txt' % (tfin,count[i]), delimiter=',')
    size[i] = my_data[0:tfin+1,1]

means = []
low = []
up = []
iqr_low = []
iqr_high = []
for i in range(len(time)):
    s = []
    for j in range(len(count)):
        s.append(size[j][i])
    
    s_work = np.array(s)
    means.append(np.mean(s_work))
    low.append(np.percentile(s_work,5))
    up.append(np.percentile(s_work,95))
    iqr_low.append(np.percentile(s_work,25))
    iqr_high.append(np.percentile(s_work,75))

time = np.arange(0,80+1,1)


plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

plt.semilogy(time,means[0:len(time)],'o',color='gray',markersize = 6)
plt.semilogy(t,I_tot,color='black',linewidth=3)
plt.plot(t,I_plot,color='C0',linewidth=3)
plt.plot(t,np.asarray(I_plot)*p_surv,color='C0',linewidth=4,linestyle='dotted')
plt.plot(t,I_det,color='black',linewidth=4,linestyle='dotted')
plt.ylim((10**0,1000))
plt.xlim((0,80))
plt.fill_between(time,low[0:len(time)],up[0:len(time)],alpha=0.25,color='C0')
plt.fill_between(time,iqr_low[0:len(time)],iqr_high[0:len(time)],alpha=0.25,color='C0')
plt.tick_params(axis='both', which='major', labelsize=15, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=10, width=1, length=5)
plt.savefig("pres_blank.png")
plt.show()






